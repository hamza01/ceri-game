import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private _http: HttpClient) { }

  
  getHistorique(): Observable<any> {
    return this._http.get('http://pedago.univ-avignon.fr:3041/voirHist',{withCredentials: true});
  }

  getUserInfo(): Observable<any> {
    return this._http.get('http://pedago.univ-avignon.fr:3041/userInfo',{withCredentials: true});
  }



}
