import { Component, Input, OnInit, ViewChild, ElementRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuestionService } from '../question.service' ;
import { QuizzComponent } from '../theme/theme.component' ;
import {ActivatedRoute, Router} from '@angular/router';
import { ThemeService } from '../theme.service';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css'],
  providers: [QuizzComponent]
})


export class QuestionComponent implements OnInit {

  @ViewChild('fondovalor') fondovalor: ElementRef;

    listQuestions: any;
    _question: QuestionService;   
    _quizz:QuizzComponent;
    theme;
    response;
    public reponse:any;
    public arrayReponse = [];
    time=1;

    constructor(private question: QuestionService, private quizz:QuizzComponent , private http: HttpClient, private router: Router, private route: ActivatedRoute) {
        this._question = question;
        this._quizz = quizz ;
        this.theme = this.route.snapshot.queryParamMap.get('theme');
    }
    
    ngOnInit(): void {
      this.sendThemeQuestion();
      setInterval(function(){ this.time++ ;}.bind(this), 1000);
    }
    

    
   
  sendThemeQuestion(): any{
        const theme = this.theme;
        console.log(theme);
        this._question.VerifThemeQuestionsTake(theme).subscribe(
          data => {
                  this.listQuestions = data ;
                 console.log(this.listQuestions);
          },error => {
                  console.log("there is an error in sendThemeQuestion function")
          }
        )
  } 

  /* lorsque user terminer le quizz il click sur un button au dessous de la quiiz et ce button appel cette fonction qui est
     envoyer la liste des reponses,theme et le timer au server
  */
  choixProposition(){
    
    console.log("choixProposition clicked");
    console.log(this.theme);
    this._question.postThemeQuestions(this.arrayReponse,this.theme,this.time).subscribe(
      data => {
            
             console.log("array des reponses : "+ this.arrayReponse+ ", theme : " +this.theme);
             console.log("data send with success");
      },error => {
              console.log("there is an error in choixProposition function")
      }
    )
    
    this.router.navigate(['/profile']);

    
  }
  //lorsque user choisie un reponse cette fonction stocke cette reponse dans une liste
  choixPropositionListe(proposition){

    console.log("choixPropositionListe clicked");
    console.log("votre reponse est : " + proposition);
    this.arrayReponse.push(proposition);
    console.log("length of array : "+ this.arrayReponse.length);
    
  }
    


}