import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(private _http: HttpClient) { }

  Veriftheme() : Observable<any>  {
    return this._http.get('http://pedago.univ-avignon.fr:3041/themeList',{withCredentials: true});
    } 

}
