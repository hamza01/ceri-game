import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ThemeService } from '../theme.service' ;
import { QuestionService } from '../question.service' ;
import {Router} from '@angular/router';


@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css']
})



export class QuizzComponent implements OnInit {

    list: string[];
    listTakeTheme: string[];
    _theme: ThemeService ;   
    _question: QuestionService;  
    public str:any;

    constructor(private theme: ThemeService, private http: HttpClient, private router: Router) {
        this._theme = theme;
    }
    
    ngOnInit(): void {
      this.afficheTheme();
    }
    
    afficheTheme(){
      
      this._theme.Veriftheme().subscribe(
        data => {
                
                this.list = data as string[];
        },error => {
                console.log("there is an error")
        }
      )
    }  
     

    choixTheme(){
      console.log("votre theme : " + this.str);
      this.router.navigate(['/questions'], { queryParams: { theme: this.str } });;
      return this.str;
    } 



}