import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class QuestionService {

   
  
  constructor(private _http: HttpClient) {
   }

    VerifThemeQuestions() : Observable<any>  {
      return this._http.get('http://pedago.univ-avignon.fr:3041/themeQuestion');
    }   

    VerifThemeQuestionsTake(theme) : Observable<any> {
       
        return this._http.get('http://pedago.univ-avignon.fr:3041/themeQuestion/'+theme);
    }
    
    postThemeQuestions(reponse,theme,timer) : Observable<any> {
       
      return this._http.post('http://pedago.univ-avignon.fr:3041/reponse/'+theme,{reponse,timer}, {withCredentials: true});
    }
  


}
