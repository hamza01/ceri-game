import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter  } from '@angular/core';

@Component({
    selector: 'app-bandeau',
    templateUrl: './bandeau.component.html',
    styleUrls: ['./bandeau.component.css'],
})

export class BandeauComponent implements OnInit {

    //messageIn contient le message de bandeau
    @Input() messageIn : string;
    messageOut : string;
    @Output('messageOutBandeau')
    sendMessageEmitter: EventEmitter<string> = new EventEmitter<string>();
    onMessageChange() {
        this.sendMessageEmitter.emit(this.messageOut);  
        }

    constructor() {} 
    ngOnInit(): void {} 

    }