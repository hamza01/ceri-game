import { Component } from '@angular/core';
import { Authentification } from '../quizz.service';
import { ProfileService } from '../profile.service';


@Component({
     selector: 'app-profile',
     templateUrl: 'profile.component.html',
     styleUrls: ['./profile.component.css']
     })
export class ProfileComponent {
    
    private _profile: ProfileService;
    listHistorique: string[];
    listInfo: string[];

    constructor(private accountService: Authentification,private profile: ProfileService) {
        this._profile = profile;
    }

    ngOnInit(): void {
        this.afficheHistorique();
        this.afficheUserInfo();
      }

      //focntion pour afficher l'historique  d'utilisateur
      afficheHistorique(){
      
        this._profile.getHistorique().subscribe(
          data => {
                  console.log(JSON.parse(JSON.stringify(data)));
                  this.listHistorique = JSON.parse(JSON.stringify(data));
                  console.log("historique infos : " + this.listHistorique);
                  console.log("here")
          },error => {
                  console.log("there is an error in afficheHistorique() function ")
          }
        )
      } 
      
      //focntion pour afficher les infos perso d'utilisateur
      afficheUserInfo(){
      
        this._profile.getUserInfo().subscribe(
          data => {
                  console.log(JSON.parse(JSON.stringify(data)));
                  this.listInfo = JSON.parse(JSON.stringify(data));
                  console.log("user infos : " + this.listInfo);
                  console.log("here")
          },error => {
                  console.log("there is an error in afficheHistorique() function ")
          }
        )
      }
      





}