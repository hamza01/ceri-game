import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BandeauComponent } from './bandeau/app.bandeau';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { Authentification } from './quizz.service';
import { ThemeService } from './theme.service';
import {APP_BASE_HREF} from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { QuizzComponent } from './theme/theme.component';
import { QuestionComponent } from './questions/question.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BandeauComponent,
    HomeComponent,
    ProfileComponent,
    QuizzComponent,
    QuestionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule 
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [Authentification, {provide: APP_BASE_HREF, useValue: '/'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
