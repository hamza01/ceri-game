import { Component } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Authentification } from './quizz.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title : string = 'appQuizz';
  auth : Authentification;
  
  constructor(_auth : Authentification,_http: HttpClient){
    this.auth = _auth;
  }

  onMessageOutBandeau = function(data : string) : void {
    alert(data);
    };
  
}
