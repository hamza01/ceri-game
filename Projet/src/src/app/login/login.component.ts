import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Authentification } from '../quizz.service' ;
import {Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  isLogged: any;
  username: string;
  password: string;
  _auth: Authentification;

  constructor(private auth: Authentification, private htttp: HttpClient, private router: Router,) {
    this._auth = auth;
  }

  ngOnInit(): void {

  }

    
  login(formConnex){
    this._auth.VerifyId(formConnex.form.value.username, formConnex.form.value.password).subscribe(

    data => {
      
      this.isLogged = data["connected"];
      //il va verfier si connected est true 
      if (this.isLogged == true) {
        //ce condition pour que si user il a deja un last connexion il va l'afficher sinon il va garder la connexion de la premier fois pour l'afficher apres
        if(localStorage.getItem("date")){
          this._auth.bandeauInfo = "Bienvenue " + formConnex.form.value.username+" Derniere Connexion: "+localStorage.getItem("date");
          this.router.navigate(['/home']);
        }
        else {
        this._auth.bandeauInfo = "Bienvenue " + formConnex.form.value.username;
          localStorage.setItem("date", new Date().toDateString())
          this.router.navigate(['/home']);
        }
      }
      else {
        this._auth.bandeauInfo = "Connexion echouee !";
      }
    },
    error => {
    }

  );

  }

    
}        
