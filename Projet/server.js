const http = require('http');
const express= require('express');
const app = express();
const path = require ('path');
var bodyParser = require('body-parser');
const router = express.Router();
const sha1 = require('sha1');
app.locals.moment = require('moment');
const session = require('express-session');
const MongoDBStore = require("connect-mongodb-session")(session);
const MongoClient = require('mongodb').MongoClient; 
var ObjectID = require('mongodb').ObjectID; 
var cors = require('cors');




app.use(function(req, res, next) {
  const origin = req.headers.origin;
  if(origin){
    res.setHeader('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  }
  next();
});



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}) );

const mdb = "mongodb://127.0.0.1:27017/db";

app.use(require('express-session')({

        secret: 'ma phrase secrete',
        saveUninitialized: true, 
        resave: false,
        store : new MongoDBStore({
              uri: "mongodb://127.0.0.1/db",
              collection: "mySessions3041",
        }),
        cookie : {maxAge : 24 * 3600 * 1000}
}));




  
  //reconnaître l'objet de requête entrant 
  app.use(express.urlencoded({
		extended:true 
  }))


  app.get('/' , (req,res)=>{
    res.redirect('/login');
   });

  //le renvoi du fichier index.html
  app.get('/login' , (req,res)=>{
   	res.sendFile(path.join(__dirname + '/index.html'));
   });

  


  //connexion à la base de donnees 
  var pg = require('pg');
  var db = 'pg://uapv2000692:TZAJzY@pedago01c.univ-avignon.fr/etd'
  
  var c = new pg.Client(db);
	    c.connect(function(err) {
       	  if (err) throw err;
      	  console.log("Connected!");
      });


  app.get('/home' , (req,res)=>{

      if(req.session.mail){
        console.log(req.session); 
        res.sendFile(path.join(__dirname + '/home.html'));
      }
      else{
        res.redirect('/login');
      }
      
   });

  //recuperer les donnees saisi dans les inputs et affichent-les dans le console
app.post('/login',(req,res)=>{
      console.log(req.body);
    	var mail = req.body.user;
      var mdp = req.body.password;
      var mdpcrypter = sha1(mdp);
      console.log(mail+' '+mdp);
  
      var responseData = {};
      
      var ident = c.query('SELECT * FROM fredouil.users WHERE identifiant = $1 AND motpasse = $2', [mail,mdpcrypter], function(error, results) {
            
        sqlStatus = "UPDATE fredouil.users set statut_connexion ='1' WHERE identifiant='" + mail + "';";

              if (results.rows[0]) {
                
          
                  req.session.mail = mail;
                  req.session.dateConnexion=app.locals.moment().format('MM/DD/YYYY, h:mm:ss a');
                  console.log(req.session);
                  responseData.firstname = results.rows[0].nom;
                  responseData.image = results.rows[0].avatar;
                  responseData.id = results.rows[0].id;
                  req.session.iduser = responseData.id;
                  req.session.name = responseData.firstname;
                  res.json({connected: true});
                  console.log("id : "+ responseData.id  + " nom " + responseData.firstname +  " avatar " + responseData.image);

                  //change mon status de connexion de 0 à 1
                  c.query(sqlStatus, function(error, result2) {
                    if (error) { 
                      console.log('Erreur d’exécution de la requete' + error.stack);
                    }
                  })

              } else {
                res.json({connected: false})
                  console.log("Identifiant et/ou mot de passe sont Incorrecte ");
              }			

       

      });

  } 

); 



app.get('/logout' , (req,res)=>{
    res.sendFile(path.join(__dirname + '/index.html'));
   });


app.post('/index', function (req, res) {
 
            req.session.destroy();
            console.log(req.session);
            res.json({connected: false});
            console.log("logout success!");      

});


/*
Fonction qui s'execute a l'acces de /themeList
elle fais un find dans la BDD MongoDB pour recuperer tout les themes
*/
app.get('/themeList', function(req, res) {
  console.log("-------------------------------------");
  console.log("Acces a /ThemeList");

  console.log("session id : "+ req.session.cookie.mail);
  console.log("session : "+ JSON.stringify(req.session.cookie));

  MongoClient.connect(mdb, {
    useNewUrlParser: true
  }, function(err, mongoClient) {
    if (err) {
      return console.log("erreur connexion base de données");
    }
    if (mongoClient) {
      mongoClient.db().collection('quizz').distinct('thème', function(err, data) {
        if (err) return console.log('erreur base de données');
        if (data) {
          console.log('requete ok pour les themes');
          mongoClient.close();
          res.send(data);
        }
      });
    }
  });
});

/*
Fonction qui s'execute a l'acces de /ThemeQuestion/:theme
elle fais un find dans la BDD MongoDB sur le theme choisi.

*/
app.get('/themeQuestion/:theme', function(req, res) {
  console.log("-------------------------------------");
  console.log("Acces a /ThemeQuestion/:theme");

  var {theme} = req.params;
  
  MongoClient.connect(mdb, {
    useNewUrlParser: true
  }, function(err, mongoClient) {
    if (err) {
      return console.log("erreur connexion base de données");
    }
    if (mongoClient) {
      mongoClient.db().collection('quizz').find({
        'thème': theme
      }, {
        "quizz": 1,
        _id: 0
      }).toArray(function(err, data) { // DATA RESTE VIDE
        if (err) return console.log('erreur base de données');
        if (data) {
          console.log('requete ok pour les questions de theme : '+ theme);
          mongoClient.close();
          res.send(data);
        }
      });
    }
  });
});

/*
Fonction qui s'execute a l'acces de /reponse/:theme
elle fais un find dans la BDD MongoDB sur le theme choisi et la reponse choisi, apres elle recupere ces donnes 
et elle fais une comparaison entre les reponses entrant par le cote client et les reponses recuperer par find
*/
app.post('/reponse/:theme', function(req, res) {
  console.log("-------------------------------------");
  console.log("Acces a /reponse Post");

  var {theme} = req.params;
  var reponse = req.body.reponse ;
  var niveauJeu = 2 ;
  var userid = req.session.iduser;
  var time = req.body.timer;
  
  MongoClient.connect(mdb, {
    useNewUrlParser: true
  },  function(err, mongoClient) {
    if (err) {
      return console.log("erreur connexion base de données");
    }
    if (mongoClient) {
       mongoClient.db().collection('quizz').find({
        'thème': theme, 'quizz.réponse': {$in : reponse}
      }, {
        "quizz": 1,
        _id: 0
      }).toArray(function(err, data) { 
        if (err) return console.log('erreur base de données');
        if (data) {
          let iteration = 0;
          let nbCorrectResponses = 0;
          let score = 0 ;
          let nbQuestion = 30;
          data.forEach(dataElement => { 
            dataElement.quizz.forEach(dataElementReponse => { 
              //faire la comparaison entre la liste des reponse qui est existe sur le mongodb et la liste des reponse de user
                  if(reponse.indexOf(dataElementReponse["réponse"]) >= 0 ){
                      nbCorrectResponses++;
                      
                  }
                  iteration++;
             })    
            });

          console.log("nombre des reponse correctes : "+ nbCorrectResponses);
          //methode de calcule de score 
          score = ((nbCorrectResponses/nbQuestion) * time) + 1000;
          if (score > 0) {
            console.log("score 2: "+ Math.floor(score))
          }
          else {
            score = 0;
          }
         
            
          console.log('requete ok pour les questions de reponse de theme : '+ theme);
         //requete pour inserer les donnes sue le tableau fredouil.historique
          sqlSend = "INSERT INTO fredouil.historique(id_user,date_jeu,niveau_jeu,nb_reponses_corr,temps,score) VALUES ('" + userid + "','" + new Date().toISOString() + "','" + niveauJeu + "','"  + nbCorrectResponses + "','" + time + "','" + Math.floor(score) + "');";
          
          if (!req.body.timer && !req.body.reponse) {
            return res.status(400).json({
              status: 'error',
              error: 'req body cannot be empty',
            });
          } else {
          c.query(sqlSend, function(err, result) {
            var responseData = {}; // creation de la reponse
            if (err) {
              console.log('Erreur d’exécution de la requete' + err.stack);
            } else if (result)
            {
              console.log('Ajout dans historique reussi');
      
              responseData.statusMsg = 'Modification reussi ';
              responseData.sucess = 'sucess';
      
            } else {
              console.log('Modification échouée');
              responseData.statusMsg = 'Modificatssion échouée';
              responseData.sucess = 'erreur';
            }
            console.log(responseData);
          })}; 
          
          mongoClient.close();
          res.send(data);
        }
      });
    }
  });
});


/*
Fonction qui s'execute a l'acces de /VoirHist, ppour afficher l'historique d'utilisateur
*/
app.get('/voirHist', function(req, res) {
  console.log("-------------------------------------");
  console.log("Acces a /VoirHist");
  histoId = req.session.iduser;
  sqlVoirHist = "select date_jeu,nb_reponses_corr,temps,score from fredouil.historique where id_user='" + histoId + "'ORDER BY id DESC LIMIT 1;";
    c.query(sqlVoirHist, function(err, result) {
      var responseData = {}; // creation de la reponse
      if (err) {
        console.log('Erreur d’exécution de la requete' + err.stack);
      } else if (result) {
        responseData.statusMsg = 'Recuperation reussi ';
        responseData.sucess = 'sucess';
        responseData.resultat = result;
      } else {
        console.log('Modification échouée');
        responseData.statusMsg = 'Recuperation échouée';
        responseData.sucess = 'erreur';
      }
      res.send(JSON.stringify(result.rows));
      console.log("data historique sent with success :  "+ JSON.stringify(result.rows))
      console.log("rows :  "+ result.rows);
    });
  
});



/*
Fonction qui s'execute a l'acces de /userInfo, ppour afficher les infos d'utilisateur
*/
app.get('/userInfo', function(req, res) {
  console.log("-------------------------------------");
  console.log("Acces a /userInfo");

  sqlGetList = "select statut_connexion,nom,prenom,avatar from fredouil.users where identifiant='" + req.session.mail+ "';";

    c.query(sqlGetList, function(err, result) {
      var responseData = {}; // creation de la reponse
      if (err) {
        console.log('Erreur d’exécution de la requete' + err.stack);
      } else if (result) {
        responseData.statusMsg = 'Recuperation reussi ';
        responseData.sucess = 'sucess';
        responseData.resultat = result;
      } else {
        console.log('Modification échouée');
        responseData.statusMsg = 'Recuperation échouée';
        responseData.sucess = 'erreur';
      }
      res.send(JSON.stringify(result.rows));
      console.log("data user sent with success :  "+ JSON.stringify(result.rows))
    });
  
});




















  

app.listen(process.env.PORT || 3041);